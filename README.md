# Tail functionality implementation in React using GraphQL Subscriptions

## Server Setup

```
cd tail-react
npm install
npm run server
```

Now, navigate the browser to http://localhost:5000/playground
The HTTP server starts on port 5000, the websocket starts at port 5001.
You may use the following subscription query to subscribe to the file "test.txt" changes

```graphql
subscription{
  fileChanged(input:{
    line_count:8
  }){
    content
    updatedAt
    line_count
  }
}
```
Try changing the contents of the file test.txt located in the home folder, the last lines will be published to the playground.


## Client Setup
```
npm run client-install
npm start
```

The react application starts at http://localhost:3000
You can input the number of lines to read and it will fetch results from the GraphQL API.
