/**
 * Created by ankur at 23/3/19 1:41 PM.
 */

import readLastLines from './read-last-lines'
import fs from 'fs'
import socket from '../graphql/socket'
import path from 'path'

// const noOfLines = process.argv[2] || 10
const noOfLines = 10

const tail = () => {

  // const filePath = '/home/ankur/IdeaProjects/test/tail-react-realtime-app/utils/test.txt'
  const filePath = path.join(__dirname, '../test.txt')

  fs.watchFile(
    filePath,
    { persistent: true, interval: 0 },
    (curr, prev) => {
      console.log(
        `File changed at: ${curr.mtime}, last ${noOfLines} lines are::`)
      readLastLines.read(filePath, noOfLines)
        .then((lines) => {
          console.log(lines)

          socket.publish('FILE_CHANGED', {
            fileChanged: {
              content: lines,
              updatedAt: curr.mtime
            }
          })
        })
    }
  )
}

export default tail
