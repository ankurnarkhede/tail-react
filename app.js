import express from 'express'
import graphqlHTTP from 'express-graphql'
import schema from './graphql'
import tail from './utils/tail'
import cors from 'cors'

const { SubscriptionServer } = require('subscriptions-transport-ws')
const { subscribe, execute } = require('graphql')
const expressPlayground = require(
  'graphql-playground-middleware-express').default

const app = express()

app.use(cors())

// registering GET / route
app.get('/', (req, res) => {
  res.send('Hello World! This is a GraphQL API')
})

// GraphQL api endpoint
app.use('/graphql', graphqlHTTP(() => ({
  schema,
  graphiql: true,
  pretty: true
})))

// Playground middleware route
app.get(
  '/playground',
  expressPlayground({
    endpoint: '/graphql',
    subscriptionsEndpoint: `ws://localhost:5001/subscriptions`
  })
)

SubscriptionServer.create({
  schema,
  execute,
  subscribe,
  onConnect: () => console.log('Subscription Client Connected')
}, {
  app,
  path: '/subscriptions',
  port: 5001
})

tail()

module.exports = app
