/**
 * Created by ankur at 13/2/19 2:15 PM.
 */
import tailResponse from '../types/tail-response'
import noOfLinesInput from '../types/no-of-lines-input'
import socket from '../socket'

module.exports = {
  type: tailResponse,
  args: {
    input: {
      type: noOfLinesInput
    }
  },
  subscribe: () => socket.asyncIterator('FILE_CHANGED')
}
