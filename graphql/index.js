
import {
  GraphQLObjectType,
  GraphQLSchema
} from 'graphql'

import queries from './queries'
import subscriptions from './subscriptions'

export default new GraphQLSchema({
  query: new GraphQLObjectType({
    name: 'query',
    fields: queries
  }),
  subscription: new GraphQLObjectType({
    name: 'subscription',
    fields: subscriptions
  })
})
