/**
 * Created by ankur at 23/3/19 3:57 PM.
 */

import tailResponse from '../types/tail-response'
import noOfLinesInput from '../types/no-of-lines-input'
import { fileData } from './resolvers'

export default {
  type: tailResponse,
  args: {
    input: {
      type: noOfLinesInput
    }
  },
  resolve: fileData
}
