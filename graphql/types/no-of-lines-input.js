/**
 * Created by ankur at 23/3/19 4:59 PM.
 */
import {
  GraphQLInt,
  GraphQLInputObjectType
} from 'graphql'

export default new GraphQLInputObjectType({
  name: 'noOfLinesInput',
  description: 'Number of last lines of file to be displayed',
  fields: () => ({
    line_count: {
      type: GraphQLInt,
      default: 10,
      description: 'Number of last lines of file to be displayed'
    }
  })
})
