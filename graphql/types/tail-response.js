/**
 * Created by ankur at 23/3/19 2:26 PM.
 */
import {
  GraphQLInt,
  GraphQLObjectType,
  GraphQLString
} from 'graphql'

import {
  GraphQLDateTime
} from 'graphql-iso-date'

export default new GraphQLObjectType({
  name: 'tailResponse',
  description: 'Tail Response',
  fields: () => ({
    line_count: {
      type: GraphQLInt
    },
    content: {
      type: GraphQLString
    },
    updatedAt: {
      type: GraphQLDateTime
    }
  })
})
